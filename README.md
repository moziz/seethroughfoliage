# See through foliage shader and controller for Unity #

Make a hole in foliage (or other geometry) that is in the line of sight betweed a camera and a player character (or what ever you attach the target component to). This is a quick and dirty solution to the problem of stuff blocking the view to the player character when the character moves behind something.

Uses Simplex noise to make the cutout hole a bit more organic looking as the hole edge blends in nicely with leafy foliage. It does not require alpha blending but rather discards fragments based on the noise pattern.

# Usage #

1. Make a material for your level geometry and use the treeDither shader.
2. Attach a TreeDitherHoleTarget component to your player character. Tweak the parameters how ever you desire.
3. Launch the game and move your character behind an obstacle and observe the cutout hole around the character.

Best results can be achieved when the noise frequency roughly matches the size of the leaves.
