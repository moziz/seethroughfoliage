﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacterController : MonoBehaviour
{
	public Transform cameraTrans;
	public float moveSpeed = 0.5f;

	void Start()
	{
	}

	void Update()
	{
		Vector3 inputDir = new Vector3();

		if (Input.GetKey(KeyCode.W))
			inputDir += new Vector3(0, 0, 1);

		if (Input.GetKey(KeyCode.S))
			inputDir += new Vector3(0, 0, -1);

		if (Input.GetKey(KeyCode.A))
			inputDir += new Vector3(-1, 0, 0);

		if (Input.GetKey(KeyCode.D))
			inputDir += new Vector3(1, 0, 0);

		Vector3 cameraOrientedInputDir = cameraTrans.rotation * inputDir;
		cameraOrientedInputDir.y = 0;
		cameraOrientedInputDir.Normalize();

		transform.position += cameraOrientedInputDir * moveSpeed * Time.deltaTime;
	}
}
