﻿using UnityEngine;
using System.Collections.Generic;

public class TreeDitherHoleTarget : MonoBehaviour {
    public List<Material> holeMaterials;
    public Camera targetCamera;

    [Range(0f, 1f)]
    public float holeRadius = 0.15f;
    public float depthLimitOffset = 2f;

    int holeCenterProperty;
    int holeRadiusProperty;
    int aspectRatioProperty;
    int depthLimitProperty;

    void Awake() {
        holeCenterProperty = Shader.PropertyToID("_HoleCenter");
        holeRadiusProperty = Shader.PropertyToID("_HoleRadius");
        aspectRatioProperty = Shader.PropertyToID("_AspectRatio");
        depthLimitProperty = Shader.PropertyToID("_DepthLimit");
    }
    
	void Update() {
        // Calculate hole center in screen space
        Vector2 screenPosition = targetCamera.WorldToScreenPoint(transform.position);
        screenPosition.x = screenPosition.x / (float)targetCamera.pixelWidth;
        screenPosition.y = screenPosition.y / (float)targetCamera.pixelHeight;

        // Calculate screen aspect ratio
        float w = (float)targetCamera.pixelWidth;
        float h = (float)targetCamera.pixelHeight;
        float screenAspectRatio = w / h;

        // Calcluate the distance from the camera to the player model
        float depthLimit = (targetCamera.transform.position - transform.position).magnitude + depthLimitOffset;

        // Apply properties
        foreach (Material mat in holeMaterials) {
            mat.SetVector(holeCenterProperty, new Vector4(screenPosition.x, screenPosition.y, 0f, 0f));
            mat.SetFloat(aspectRatioProperty, screenAspectRatio);
            mat.SetFloat(holeRadiusProperty, holeRadius);
            mat.SetFloat(depthLimitProperty, depthLimit);
        }
	}
}
