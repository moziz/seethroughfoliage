// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Nature/Tree Dither" {
	Properties {
		_MainTex ("Main Texture", 2D) = "white" {  }
		_Cutoff ("Alpha cutoff", Range(0, 1)) = 0.5
        _HoleCenter ("Hole center", Vector) = ( 0.0, 0.0, 0.0, 0.0 )
        _HoleRadius ("Hole radius", Float) = 0.25
        _AspectRatio ("Screen aspect ratio", Float) = 1.0
        _DepthLimit("Hole depth limit", Float) = 20.0
        _DepthFadeDistance("Hole depth fade distance", Float) = 2.0
        _NoiseFrequency("Noise frequency", Float) = 48.0
	}
	
	SubShader {	
        Tags { "RenderType" = "Opaque" }

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

            //
            // Description : Array and textureless GLSL 2D simplex noise function.
            //      Author : Ian McEwan, Ashima Arts.
            //  Maintainer : ijm
            //     Lastmod : 20110822 (ijm)
            //     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
            //               Distributed under the MIT License. See LICENSE file.
            //               https://github.com/ashima/webgl-noise
            // 

            float3 mod289(float3 x) {
                return x - floor(x * (1.0 / 289.0)) * 289.0;
            }

            float2 mod289(float2 x) {
                return x - floor(x * (1.0 / 289.0)) * 289.0;
            }

            float3 permute(float3 x) {
                return mod289(((x*34.0) + 1.0)*x);
            }

            float snoise(float2 v) {
                const float4 C = float4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                    0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                    -0.577350269189626,  // -1.0 + 2.0 * C.x
                    0.024390243902439); // 1.0 / 41.0
                                        // First corner
                float2 i = floor(v + dot(v, C.yy));
                float2 x0 = v - i + dot(i, C.xx);

                // Other corners
                float2 i1;
                //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
                //i1.y = 1.0 - i1.x;
                i1 = (x0.x > x0.y) ? float2(1.0, 0.0) : float2(0.0, 1.0);
                // x0 = x0 - 0.0 + 0.0 * C.xx ;
                // x1 = x0 - i1 + 1.0 * C.xx ;
                // x2 = x0 - 1.0 + 2.0 * C.xx ;
                float4 x12 = x0.xyxy + C.xxzz;
                x12.xy -= i1;

                // Permutations
                i = mod289(i); // Avoid truncation effects in permutation
                float3 p = permute(permute(i.y + float3(0.0, i1.y, 1.0))
                    + i.x + float3(0.0, i1.x, 1.0));

                float3 m = max(0.5 - float3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
                m = m*m;
                m = m*m;

                // Gradients: 41 points uniformly over a line, mapped onto a diamond.
                // The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

                float3 x = 2.0 * frac(p * C.www) - 1.0;
                float3 h = abs(x) - 0.5;
                float3 ox = floor(x + 0.5);
                float3 a0 = x - ox;

                // Normalise gradients implicitly by scaling m
                // Approximation of: m *= inversesqrt( a0*a0 + h*h );
                m *= 1.79284291400159 - 0.85373472095314 * (a0*a0 + h*h);

                // Compute final noise value at P
                float3 g;
                g.x = a0.x  * x0.x + h.x  * x0.y;
                g.yz = a0.yz * x12.xz + h.yz * x12.yw;
                return 130.0 * dot(m, g);
            }
		
			struct appdata {
			    float4 pos : POSITION;
				float3 normal : NORMAL;
			    //fixed4 color : COLOR;
			    float4 texcoord : TEXCOORD0;
			};

            struct v2f {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float4 sp : TEXCOORD1;
            };

            v2f vert(appdata v) {
				v2f o;
                o.pos = UnityObjectToClipPos(v.pos);
                o.uv = v.texcoord;
                o.sp = ComputeScreenPos(o.pos);
				return o;
			}
			
			sampler2D _MainTex;
            float _Cutoff;
            float4 _HoleCenter;
            float _HoleRadius;
            float _AspectRatio;
            float _DepthLimit;
            float _DepthFadeDistance;
            float _NoiseFrequency;
		    
			fixed4 frag(v2f i) : SV_Target {
				float2 screenPos = (i.sp.xy / i.sp.w);
                float2 holePos = _HoleCenter.xy;
                float2 distance = holePos - screenPos;
                
                screenPos.x *= _AspectRatio;
                distance.x *= _AspectRatio;

                distance.x = abs(distance.x);
                distance.y = abs(distance.y);

                if (_AspectRatio < 1.0f) {
                    float temp = distance.x;
                    distance.x = distance.y;
                    distance.y = temp;
                }

                float ditherValue = (snoise(screenPos * _NoiseFrequency) + 2.0f) / 2.0f;
                
                if (
                    i.sp.z + ditherValue * (_DepthFadeDistance / (i.sp.z + _DepthFadeDistance)) < _DepthLimit && // Distance fade
                    (   // Round hole
                        ditherValue > distance.x / (sin(distance.x / length(distance)) * _HoleRadius) &&
                        ditherValue > distance.y / _HoleRadius
                    )
                ) {
                    discard;
                }

                fixed4 texcol = tex2D(_MainTex, i.uv);
                clip(texcol.a - _Cutoff);

                return texcol;
			}
			ENDCG
		}
	}
}
